import User.User;
import User.UserEx;

public class Main {
    public static void main(String[] args) {
        UserEx user1 = new UserEx();
        user1.setFirstName("QWERTY");
        user1.setLastName("qwerty");
        user1.setEmail("asd@ffg");
        user1.setMobilePhone(1234);
        user1.setWorkPhone(2134);
        user1.setPassword("123567891111111");
        user1.setExperience(3);
        user1.setSalary(100);
        System.out.println("First Name - " + user1.getFirstName());
        System.out.println("Last Name - " + user1.getLastName());
        System.out.println("Email - " + user1.getEmail());
        System.out.println("Mobile Phone - " + user1.getMobilePhone());
        System.out.println("Work Phone - " + user1.getWorkPhone());
        System.out.println("Password - " + user1.getPassword());
        System.out.println("Salary - " + user1.getSalary());
        System.out.println("Experience - " + user1.getExperience());

    }

}

