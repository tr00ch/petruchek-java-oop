package User;

public class User {
    private String firstName;
    private String lastName;
    private int workPhone;
    private int mobilePhone;
    private String email;
    private String password;

    /*public User(String firstName, String lastName, int workPhone, int mobilePhone, String email, String password ){
        this.firstName = firstName;
        this.lastName = lastName;
        this.workPhone = workPhone;
        this.mobilePhone = mobilePhone;
        this.email = email;
        this.password = password;
    }*/

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(int workPhone) {
        this.workPhone = workPhone;
    }

    public int getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(int mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return emailCheck() ? email : "ERROR!! no @";
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return passCheck() ? password : "ERROR!! Password should be 8 to 16 characters";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private boolean emailCheck() {
        return email.contains("@");
    }

    private boolean passCheck() {
        return password.length() >= 8 & password.length() <= 16;
    }


}





